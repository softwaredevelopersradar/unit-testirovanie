﻿

using NUnit.Framework;

namespace ОбзорВозможностей.Tests
{
    [TestFixture]
    public class HumanTest
    {
        [Test]
        public void CheckNegativeCoordInMoveToMethod()
        {
            Coord coord = new Coord();
            coord.lat = -1;
            coord.lon = -1;
            coord.alt = -1;
            Human human = new Human();

            Assert.IsFalse(human.MoveTo(coord));
        }

        [Test]
        public void CheckCorrectCoordInMoveToMethod()
        {
            Coord coord = new Coord();
            coord.lat = 1;
            coord.lon = 1;
            coord.alt = 1;
            Human human = new Human();

            Assert.IsTrue(human.MoveTo(coord));
        }

        [TestCase(120, 100, 220)]
        [TestCase(-100, 0, -100)]
        [TestCase(0, 0, 0)]
        public void CheckSumMethod(int number1, int number2, int expectedSum)
        {
            Human human = new Human();

            int sum = human.Sum(number1, number2);
            Assert.AreEqual(expectedSum, sum);
        }

        [Test]
        public void CheckAmountOfFingers_5_5()
        {
            Human human = new Human();

            var numberOfFingersOnLeftHand = human.leftHand.numberOfFingers;
            var numberOfFingersOnrightHand = human.rightHand.numberOfFingers;

            Assert.AreEqual(human.leftHand.numberOfFingers, 5);
            Assert.AreEqual(human.rightHand.numberOfFingers, 5);

        }

        [Test]
        public void CheckDevideMethod()
        {
            Human human = new Human();

            int x = 20;
            int y = 4;
            int result = 5;

            double devide = human.Devide(x, y);

            Assert.AreEqual(result, devide);
        }

        [TestCase(1, 1, 1)]
        [TestCase(8, 2, 4)]
        [TestCase(100, 4, 25)]
        public void CheckDevideMethod(int number1, int number2, int expectedDev)
        {
            Human human = new Human();

            double dev = human.Devide(number1, number2);
            Assert.AreEqual(expectedDev, dev);
        }

        [Test]
        public void CheckMultiplicationMethod()
        {
            Human human = new Human();

            int x = 5;
            int y = 5;
            int result = 25;

            int product = human.Multiplication(x, y);

            Assert.AreEqual(product, result);

        }

        [TestCase(1, 1, 1)]
        [TestCase(8, 2, 16)]
        [TestCase(100, 4, 400)]
        public void CheckMultiplicationMethod(int number1, int number2, int product)
        {
            Human human = new Human();

            int expectedMult = human.Multiplication(number1, number2);
            Assert.AreEqual(expectedMult, product);
        }
    }   
}


