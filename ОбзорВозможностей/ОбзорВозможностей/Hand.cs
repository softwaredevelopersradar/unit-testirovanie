﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ОбзорВозможностей
{
    public class Hand
    {
        public int numberOfFingers = 5;

        public bool Touch(object obj)
        {
            return true;
            // логика косания
        }

        public object TakeObject(object obj)
        {
            Touch(obj);
            // чёто сделали с ним
            return obj;
        }
    }
}
