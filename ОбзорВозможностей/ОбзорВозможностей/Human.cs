﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ОбзорВозможностей
{
    public class Human
    {
        public Hand leftHand;
        public Hand rightHand;
        Leg leftLeg;
        Leg rightLeg;

        public Human()
        {
            this.leftHand = new Hand();
            this.rightHand = new Hand();
            leftLeg = new Leg();
            rightLeg = new Leg();
        }

        public bool MoveTo(Coord coord)
        {
            if(coord.lon < 0 || coord.lat < 0 || coord.alt < 0)
            {
                return false;
            }

            leftLeg.MoveTo(coord);
            rightLeg.MoveTo(coord);
            return true; 
        }

        /// <summary>
        /// Суммирование двух чисел.
        /// </summary>
        /// <param name="number1">Число 1</param>
        /// <param name="number2">Число 2</param>
        /// <returns>Сумма чисел</returns>
        public int Sum(int number1, int number2)
        {
            return number1 + number2;
        }

        /// <summary>
        /// Деление двух чисел 
        /// </summary>
        /// <param name="number">Делимое</param>
        /// <param name="devider">Делитель</param>
        /// <returns>Частное</returns>
        public double Devide(int number, int devider)
        {
            return (double)number / devider;
        }

        /// <summary>
        /// Умножение двух чисел
        /// </summary>
        /// <param name="number1">Число 1</param>
        /// <param name="number2">Число 2</param>
        /// <returns>Произведение</returns>
        public int Multiplication(int number1, int number2)
        {
            return number1 + number2;
        }
    }
}
